Hier finden Sie den kompletten Code zum Tutorial **"Eine Simple ToDo App mit WebSQL"**.

In diesem Tutorial wird eine Javascript Anwendung gebaut die ihre Daten in einer WebSQL Datenbank speichert.

Das Tutorial finden Sie unter diesem [Link](https://apps-mit-web.de/eine-simple-todo-app-mit-websql/)

In diesem Projekt werden die folgenden externen Bibliotheken verwendet:

* [jQuery](https://jquery.com/) Lizenz: [MIT License](https://jquery.org/license/)
* [Bootstrap](http://getbootstrap.com/) Lizenz: [MIT License](https://github.com/twbs/bootstrap/blob/master/LICENSE)

Das Projekt wurde unter der folgenden Lizenz veröffentlicht:

The MIT License (MIT)

Copyright (c) 2015 AppsMitWeb

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.