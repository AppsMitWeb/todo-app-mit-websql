"use strict";

var database = 
{	
	db: null,

	/**
	* Konstruktor
	* Wenn noch keine Datenbakn vorhanden ist wird sie erst angelegt und zum lesen/schreiben geöffent
	* Sollte die Databank schon bestehen wird sie zum lesen/schreiben geöffnet
	*
	* @method initialize
	*/
	initialize: function()
	{	
		/* 
		Parameter:
		1. Name der Datenbank
		2. Versionsnummer
		3. Beschreibung
		4. Größe der Datenbank
		*/
		this.db = openDatabase( 'todo_app', '1.0', 'Todo App database',  512000 );

		if( this.db != null )
		{

			this.db.transaction( 

				function(tx) {

	    			tx.executeSql( 'CREATE TABLE IF NOT EXISTS task ( task_id INTEGER PRIMARY KEY, task_name TEXT )', [], database.on_succes, database.on_error );
	  			}
	  		);
		}	
	},

	/**
	* Diese Funktion fügt eine neue Aufgabe der Taballe task hinzu
	*
	* @method insert_task
	* @param {String} task Aufgabe die eingetragen werden soll
	* @param {Function} succes_callback Callback der aufgerufen wird wenn die Interaktion mit der Datenbank erfolgreich war
	* @param {Function} error_callback Callback der aufgerufen wird wenn ein Fehler in Datenbankabfrage auftritt
	*/
	insert_task: function( task, succes_callback, error_callback )
	{
		if( this.db != null )
		{
			this.db.transaction( 

				function(tx) {

					tx.executeSql('INSERT INTO task ( task_name ) VALUES ( ? )', [ $.trim( task ) ], succes_callback,  error_callback );

  				}
  			);
		}
		else
		{
			console.log( 'database is close -- function: insert_task' );
		}
	},

	/**
	* Diese Funktion liest alle Aufgaben aus der Datenbank
	*
	* @method select_all_task
	* @param {Function} succes_callback Callback der aufgerufen wird wenn die Interaktion mit der Datenbank erfolgreich war
	* @param {Function} error_callback Callback der aufgerufen wird wenn ein Fehler in Datenbankabfrage auftritt
	*/
	select_all_task: function( succes_callback, error_callback )
	{
		if( this.db != null )
		{
			this.db.transaction( 

				function(tx) {

					tx.executeSql('SELECT * FROM task', [], succes_callback,  error_callback );

  				}
  			);
		}
		else
		{
			console.log( 'database is close -- function: select_all_task' );
		}
	},	

	/**
	* Diese Funktion liest eine bestimmte Aufgabe aus der Datenbank
	*
	* @method select_task
	* @param {Number} task_id Die ID der Aufgabe
	* @param {Function} succes_callback Callback der aufgerufen wird wenn die Interaktion mit der Datenbank erfolgreich war
	* @param {Function} error_callback Callback der aufgerufen wird wenn ein Fehler in Datenbankabfrage auftritt
	*/
	select_task: function( task_id, succes_callback, error_callback )
	{
		if( this.db != null )
		{
			task_id = parseInt(task_id);

			if( !isNaN( task_id ))
			{
				this.db.transaction( 

					function(tx) {

						tx.executeSql('SELECT * FROM task WHERE task_id = ? ', [ task_id ], succes_callback,  error_callback );

	  				}
	  			);
	  		}
	  		else
	  		{
	  			console.log( 'id is not a number -- function: select_task' );		
	  		}
		}
		else
		{
			console.log( 'database is close -- function: select_task' );
		}
	},

	/**
	* Diese Funktion löscht eine bestimmte Aufgabe aus der Datenbank
	*
	* @method delete_task
	* @param {Number} task_id Die ID der Aufgabe
	* @param {Function} succes_callback Callback der aufgerufen wird wenn die Interaktion mit der Datenbank erfolgreich war
	* @param {Function} error_callback Callback der aufgerufen wird wenn ein Fehler in Datenbankabfrage auftritt
	*/
	delete_task: function( task_id, succes_callback, error_callback )
	{
		if( this.db != null )
		{
			task_id = parseInt(task_id);

			if( !isNaN( task_id ))
			{
				this.db.transaction( 

					function(tx) {

						tx.executeSql('DELETE FROM task WHERE task_id = ? ', [ task_id ], succes_callback,  error_callback );

	  				}
	  			);
			}
			else
			{
				console.log( 'task id is not a number -- function: delete_task' );	
			}
			
		}
		else
		{
			console.log( 'database is close -- function: delete_task' );
		}
	},

	/**
	* Allgemeiner Callback für den Fehlerfall
	*
	* @method on_error
	*/
	on_error: function( tx, error )
	{
        console.log( 'error: ' )
		console.log( error.message );
	},

	/**
	* Erfolgreicher allgemeiner Callback
	*
	* @method on_succes
	*/
	on_succes: function()
	{
		console.log( 'succes' );
	}
}