"use strict";

var ui =
{	

	duration: 2500, // 2500 ms  => 2.5 seconds

	/**
	* Diese Funktion fügt eine neu eingeben Aufgabe der Datenbank hinzu
	*
	* @method add_new_task
	*
	*/
	add_new_task: function()
	{	
		var $new_task = $('#new_task');

		var value = $new_task.val();

		// keine leeren Einträge
		if( value.length > 0 )
		{
			$new_task.val(''); // Input-Feld leeren

			database.insert_task( value, this.succes_insert, this.error_insert );
		}
		else
		{
			ui.show_msg('alert-danger', '<strong>Fehler!</strong> Es werden keine leeren Aufgaben gespeichert.' );
		}
	},


	/**
	* Callback für das Erfolgreiche eintragen einer neuen Aufgabe
	*
	* @method succes_insert
	* @param {Object} tx 
	* @param {Oject} results Ergebnis der Datenabfrage
	*/
	succes_insert: function( tx, results )
	{
		ui.get_all_task(); // Aktualisiert die Oberfläche
	},


	/**
	* Callback für den Fehlerfall beim eintragen einer neuen Aufgabe
	*
	* @method error_insert
	* @param {Object} tx 
	* @param {Oject} error Fehlermeldung
	*/
	error_insert: function( tx, error )
	{
		ui.show_msg('alert-danger', '<strong>Fehler!</strong> Aufgabe konnte nicht gespeichert werden.'  );
	},

	/**
	* Diese Funktion veranlasst die Datenbank eine bestimmte Aufgabe auszulesen
	*
	* @method get_task
	* @param {Number} task_id ID der Aufgabe
	*/
	get_task: function( task_id )
	{
		database.select_task( task_id ,this.success_select, this.error_select );
	},


	/**
	* Diese Funktion veranlasst die Datenbank alle Aufgabe auszulesen
	*
	* @method get_all_task
	*
	*/
	get_all_task: function()
	{
		database.select_all_task( this.success_select, this.error_select );
	},

	/**
	* Callback für das Erfolgreiche ermitteln der Daten der Aufgabe
	*
	* @method success_select
	* @param {Object} tx 
	* @param {Oject} results Ergebnis der Datenabfrage
	*/
	success_select: function( tx, results )
	{
		var len = results.rows.length;

		var tmp = '';

        for (var i = 0; i < len; i++)
        {	
			var item = results.rows.item(i);
        	
            tmp += '<a href="#" class="list-group-item clearfix" >';
		
				tmp += $.trim( item.task_name  );

				tmp += '<span class="pull-right">';
              		
              		tmp += '<button class="btn btn-xs btn-danger" onclick="ui.del_task('+ item.task_id +')">';
                		
                		tmp += '<span class="glyphicon glyphicon-trash"></span>';
              		
              		tmp += '</button>';

            	tmp += '</span>';

			tmp += '</a>'; 
        }

        $('#task_list').html( tmp );
	},


	/**
	* Callback für den Fehlerfall beim ermitteln der Daten der Aufgabe
	*
	* @method error_insert
	* @param {Object} tx 
	* @param {Oject} error Fehlermeldung
	*/
	error_select: function( tx, error )
	{
		ui.show_msg('alert-danger', '<strong>Fehler!</strong> Es konnte keine Aufgabe ausgewählt werden.' );
	},

	
	/**
	* Diese Funktion veranlasst die Datenbank eine bestimmte Aufgabe zu löschen
	*
	* @method del_task
	* @param {Number} task_id ID der Aufgabe
	*/
	del_task: function( task_id )
	{	
		task_id = parseInt( task_id );

		if( !isNaN( task_id ) && task_id > 0 )
		{
			database.delete_task( task_id, this.succes_delete, this.error_delete );
		}
		else
		{
			console.log( 'id is not a number or id is smaller then 0 -- function: del_task' );	
		}
	},

	
	/**
	* Callback für das Erfolgreiche löschen einer Aufgabe
	*
	* @method succes_delete
	* @param {Object} tx 
	* @param {Oject} results Ergebnis der Datenabfrage
	*/
	succes_delete: function( tx, results )
	{	
		ui.get_all_task(); // Oberfläche aktualisieren

		ui.show_msg('alert-success', '<strong>Erfolgreich!</strong> Aufgabe wurde gelöscht.' );
	},

	/**
	* Callback für den Fehlerfall beim löschen einer Aufgabe
	*
	* @method error_delete
	* @param {Object} tx 
	* @param {Oject} error Fehlermeldung
	*/
	error_delete: function( tx, error )
	{
		ui.show_msg('alert-danger', '<strong>Fehler!</strong> Aufgabe konnte nicht gelöscht werden.' );
	},

	/**
	* Diese Funktion gibt den Benutzer eine Rückmeldung in Form einer Nachricht
	*
	* @method show_msg
	* @param {String} kind Art der Nachricht
	* @param {String} msg Text der Nachricht
	*/
	show_msg: function( kind, msg )
	{
		var $msg = $('.msg');

		$msg.html( '<div class="alert '+ kind +'" role="alert">' + msg + '</div>' );

		$msg.show();
		
		$msg.fadeOut( ui.duration );
	}

}